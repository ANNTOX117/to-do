CREATE DATABASE IF NOT EXISTS `tareas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tareas`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `importancia`
--

CREATE TABLE `importancia` (
  `codigo` int(2) NOT NULL,
  `descripcion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `importancia`
--

INSERT INTO `importancia` (`codigo`, `descripcion`) VALUES
(1, 'Bajo'),
(2, 'Medio'),
(3, 'Alto'),
(4, 'Urgente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE `tarea` (
  `id` int(10) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `fecha` varchar(20) DEFAULT NULL,
  `nivel` int(2) DEFAULT NULL,
  `visible` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`id`, `nombre`, `descripcion`, `fecha`, `nivel`, `visible`) VALUES
(1, 'Tarea 15', 'Tarea de prueba 1 fergergerge hugojajaja jejejejeje 1', '31/12/2020', 4, 0),
(2, 'Tarea 2', 'Tarea de prueba 2 edicion', '31/12/2020', 3, 0),
(3, 'Tarea 3', 'Tarea de prueba 3', '31/12/2020', 3, 0),
(4, 'd', 'd', '12/12/2123', 1, 0),
(5, 'tarea 5', 'tatea 5', '12/12/2123', 1, 0),
(6, 'tarea 6', 'tatea 6', '12/12/2123', 1, 0),
(7, 'tarea 10', 'tarea 10 jajajajaj 2', '21/12/2019', 1, 0),
(8, 'tarea 13', 'tarea 13 hugo jejejejejeje', '11/11/2020', 1, 0),
(9, 'tarea 20', 'tarea 20 4', '11/03/1234', 1, 0),
(10, 'hola', 'hugo', '02/01/2021', 2, 0),
(11, 'jejejeeje', 'jejejeje', '11/03/1234', 1, 0),
(12, 'f', 'f', '11/03/1234', 1, 0),
(13, 't', 't', '01/01/2021', 1, 0),
(14, 'g', 'g', '12/12/2123', 1, 0),
(15, 's', 's', '01/01/2021', 1, 0),
(16, 'f', 'f', '12/12/2123', 1, 0),
(17, 't', 't', '11/03/1234', 1, 0),
(18, 'h', 'h', '12/12/2123', 1, 0),
(19, 'sd', 'sd', '11/03/1234', 1, 0),
(20, '1', '1', '11/03/1234', 1, 0),
(21, 'd', 'd', '12/12/2123', 1, 0),
(22, 'g', 'g', '11/03/1234', 1, 0),
(23, 'as', 'as', '12/12/2123', 1, 0),
(24, 'd3', 'ag', '12/12/2123', 1, 0),
(25, 'v', 'v', '12/12/2123', 1, 0),
(26, 'q1', 'q1', '12/12/2123', 1, 0),
(27, 'w1', 'w1', '12/12/2123', 1, 0),
(28, 'w3', 'w3', '12/12/2123', 1, 0),
(29, 'w2', 'w2', '12/12/2123', 1, 0),
(30, 'w3', 'w3', '12/12/2123', 1, 0),
(31, '1', '1', '12/12/2123', 1, 0),
(32, '1w', '1w', '12/12/2123', 4, 0),
(33, '1w', '1w', '12/12/2123', 4, 0),
(34, 'e', 'e', '11/03/1234', 1, 0),
(35, 'afdf', 'afdf', '01/01/2021', 1, 0),
(36, 'afdf', 'afdf', '01/01/2021', 1, 0),
(37, 'hugo3', 'dgsgfg', '11/11/2020', 1, 0),
(38, 'd1', 'sgsfgd', '11/03/1234', 1, 0),
(39, 'hugo1', 'dfsdf', '01/01/2021', 1, 0),
(40, 'hugo2', 'svsv', '11/11/2020', 1, 0),
(41, 'hugo2', 'svsv', '11/11/2020', 1, 0),
(42, 'hugo3', 'hbuhbh', '11/03/1234', 1, 0),
(43, 'hugo 3', 'dfsf', '11/11/2020', 1, 0),
(44, 'hugo5', 'ffwefwef', '11/03/1234', 1, 0),
(45, 'f2', 'dsfsdfsd', '11/03/1234', 1, 0),
(46, 'e2', 'sdfsdfsdf', '02/01/2021', 1, 0),
(47, 'e3', 'dfsdfgsdg', '11/11/2020', 1, 0),
(48, 'e1', 'ffa', '11/11/2020', 1, 0),
(49, 'd1', 'sdfsdfsdf', '11/11/2020', 1, 0),
(50, 'f2', 'dfdfdf', '11/03/1234', 1, 0),
(51, 'f3', 'sdfsdfd', '01/01/2021', 1, 0),
(52, 't1', 'dfsdfdf', '11/11/2020', 1, 0),
(53, 't2', 'dfsdffsdf', '11/11/2020', 1, 0),
(54, 't3', 'asdfdsf', '11/11/2020', 1, 0),
(55, 'r1', 'afdf', '11/11/2020', 1, 0),
(56, 's1', 'asasas', '11/11/2020', 1, 0),
(57, 'q1', 'asdasdas', '11/11/2020', 1, 0),
(58, 'q1', 'asdas', '11/11/2020', 1, 0),
(59, 'q2', 'dfsdfsdf', '11/03/1234', 1, 0),
(60, 'sq2', 'asdfsf', '11/03/1234', 1, 0),
(61, 'a2', 'ssdfsdfd', '11/11/2020', 1, 0),
(62, 'a3', 'fdfdfd', '01/01/2021', 1, 0),
(63, 'a1', 'asdfsf', '11/03/1234', 1, 0),
(64, 'fg', 'dfdfg', '11/03/1234', 1, 0),
(65, 'w1', 'fsdfsdf', '11/03/1234', 1, 0),
(66, 'w2', 'dfdsfds', '12/12/2123', 1, 0),
(67, 'w3', 'sdfsdf', '11/11/2020', 1, 0),
(68, 'w4', 'dsfsdfsdf', '11/03/1234', 1, 0),
(69, 'q1', 'fdfd', '11/03/1234', 1, 1),
(70, 'a2', 'dfafa', '11/03/1234', 1, 1),
(71, 'a3', 'asffd', '11/03/1234', 1, 1),
(72, 'q4', 'fsdfsf', '11/11/2020', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `importancia`
--
ALTER TABLE `importancia`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nivel` (`nivel`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tarea`
--
ALTER TABLE `tarea`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
